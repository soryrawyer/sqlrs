use std::io::{self, Write};
use std::mem;

use regex::Regex;

const PAGE_SIZE: usize = 4096; // 4kb
const ROWS_PER_PAGE: usize = PAGE_SIZE / mem::size_of::<User>();
const TABLE_MAX_PAGES: usize = 100;
const TABLE_MAX_ROWS: usize = ROWS_PER_PAGE * TABLE_MAX_PAGES;

enum Action {
    Terminate,
    Continue,
}

enum Statement {
    Insert(Insert),
    Select(Select),
}

#[derive(Debug)]
struct Insert {
    // for now, we'll hardcode a "user" row
    user: User,
}

#[derive(Debug)]
struct User {
    id: u16,
    username: String,
    email: String,
}

// type Row = HashMap<String, Box<dyn Any>>;
type Page = Vec<User>;
type Pages = Vec<Page>;

struct UserTable {
    // our "Table" contains a list of lists of Users
    pages: Pages,
}

struct Select {}

// not gonna add the "MetaCommandResult" and "PrepareResult" enums because
// they seem close enough to Result to just use the standard Rust type (Result)

fn main() {
    println!("Hello, world!");
    loop {
        print!("sqlrs> ");
        io::stdout().flush().unwrap();
        let mut buffer = String::new();
        match io::stdin().read_line(&mut buffer) {
            Ok(_) => {
                // the underscore is hiding the number of bytes read
                let mut chars = buffer.chars();
                chars.next_back();
                buffer = String::from(chars.as_str());
                match process(buffer) {
                    Ok(Action::Terminate) => return,
                    _ => (),
                }
            }
            Err(error) => panic!("uh oh!: {}", error),
        };
    }
}

// I'm using io::Error here because I'm too lazy to use a better error type
fn process(stmt: String) -> Result<Action, ()> {
    if stmt.starts_with(".") {
        match do_meta_command(stmt) {
            Ok(Action::Terminate) => Ok(Action::Terminate),
            _ => Ok(Action::Continue),
        }
    } else {
        match prepare_statement(stmt) {
            Ok(s) => {
                execute_statement(s);
                Ok(Action::Continue)
            }
            _ => Err(()),
        }
    }
}

fn do_meta_command(stmt: String) -> Result<Action, ()> {
    match stmt.as_str() {
        ".exit" => Ok(Action::Terminate),
        _ => Err(()),
    }
}

fn prepare_statement(stmt: String) -> Result<Statement, ()> {
    match stmt.split(" ").next() {
        Some("select") => Ok(Statement::Select(Select {})),
        Some("insert") => {
            let insert_regex: Regex =
                Regex::new(r"insert (\d{1}) (\w+) ([^@\s]+@[\w\.]+)").unwrap();
            let caps = match insert_regex.captures(stmt.as_str()) {
                Some(regex) => regex,
                None => return Err(()),
            };
            println!("caps! {:?}", caps);
            println!("caps[1]! {:?}", caps[1].to_string());
            let user = User {
                id: caps[1].to_string().parse::<u16>().unwrap(),
                username: caps[2].to_string(),
                email: caps[3].to_string(),
            };
            Ok(Statement::Insert(Insert { user: user }))
        }
        _ => Err(()),
    }
}

fn execute_statement(stmt: Statement) {
    match stmt {
        Statement::Insert(i) => println!("insert: {:?}", i),
        Statement::Select(_) => println!("data not found"),
    }
}
